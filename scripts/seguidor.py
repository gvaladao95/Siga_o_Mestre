#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from collections import deque
import numpy as np
import time

class Seguidor:
    def __init__(self):
        rospy.init_node("controle_seguidor", anonymous=True)
        self.pub = rospy.Publisher("/tb3_1/cmd_vel",Twist,queue_size=10)
        rospy.Subscriber("/tb3_0/odom", Odometry, self.update_mestre)
        rospy.Subscriber("/tb3_1/odom", Odometry, self.update)
        self.pos_mestre = Pose()
        self.position = Pose()
        self.rate = rospy.Rate(1)
        self.max_vel = 0.1
        self.max_ang = 1.0

    def update(self, msg):
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x,orientation_q.y,orientation_q.z,orientation_q.w]
        (_,_,yaw) = euler_from_quaternion(orientation_list)
        self.position.x = msg.pose.pose.position.x
        self.position.y = msg.pose.pose.position.y
        self.position.theta = yaw

    def update_mestre(self,msg):
        orientation_m = msg.pose.pose.orientation
        orientation_Mlist = [orientation_m.x,orientation_m.y,orientation_m.z,orientation_m.w]
        (_,_,yaw) = euler_from_quaternion(orientation_Mlist)
        self.pos_mestre.x = msg.pose.pose.position.x
        self.pos_mestre.y = msg.pose.pose.position.y
        self.pos_mestre.theta = yaw


    def distancia_ref(self, ref_pos):
        return np.sqrt((ref_pos.x - self.position.x)**2+(ref_pos.y - self.position.y)**2)

    def controle_vel(self, ref_pos, kp=1.5):
        distancia = self.distancia_ref(ref_pos)
        controle = kp*distancia
        if abs(controle) > self.max_vel:
            controle = self.max_vel*np.sign(controle)
        return controle

    def controle_ang(self, ref_pos, kp=6):
        angle_r = np.arctan2(ref_pos.y - self.position.y,  ref_pos.x - self.position.x )
        controle = kp*(angle_r - self.position.theta)
        if abs(controle) > self.max_ang:
            controle = self.max_ang*np.sign(controle)
        return controle

    def seguir_ref(self, ref):
        ref_pos = Pose()
        ref_pos.x = ref.x
        ref_pos.y = ref.y
        tolerancia = 0.25
        vel_msg = Twist()
        while self.distancia_ref(ref_pos) > tolerancia:
            vel_msg.linear.x = self.controle_vel(ref_pos)
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel_msg.angular.z = self.controle_ang(ref_pos)
            self.pub.publish(vel_msg)
            self.rate.sleep()
        
        vel_msg.linear.x = 0
        vel_msg.angular.z = 0
        self.pub.publish(vel_msg)

    def run(self):
        posicao_mestre = Pose()
        while not rospy.is_shutdown():
            posicao_mestre.x = self.pos_mestre.x
            posicao_mestre.y = self.pos_mestre.y
            self.seguir_ref(posicao_mestre)
            self.rate.sleep()

         

if __name__ == '__main__':
    try:
        bot = Seguidor()
        bot.run()
    except rospy.ROSInterruptException:
        pass
