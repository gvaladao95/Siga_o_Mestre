#!/usr/bin/env python

import math
from math import sin, cos, pi

import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3

class controle_mestre:

    def __init__(self):
        rospy.init_node('controle_mestre_node',  anonymous=False) 
        self.pub1 = rospy.Publisher('/tb3_0/cmd_vel', Twist, queue_size=10)
        self.odom_pub = rospy.Publisher('/tb3_0/odom', Odometry, queue_size=10)
        rospy.Subscriber("/cmd_vel", Twist, self.update)
       
        self.new_pos = Twist()
        self.cmd = Twist()
        self.rate = rospy.Rate(1)

        self.broadcaster = tf.TransformBroadcaster()
        self.x = 0.0
        self.y = 0.0
        self.th = 0.0

        self.vx = 0.1
        self.vy = -0.1
        self.vth = 0.1

        self.current_time = rospy.Time.now()
        self.last_time = rospy.Time.now()
        r = rospy.Rate(5.0)
        rospy.loginfo("/cmd_vel >> /tb3_0/cmd_vel")
        
    def publica_odom(self):
        current_time = rospy.Time.now()

        dt = (current_time - self.last_time).to_sec()
        delta_x = (self.vx * cos(self.th) - self.vy * sin(self.th)) * dt
        delta_y = (self.vx * sin(self.th) + self.vy * cos(self.th)) * dt
        delta_th = self.vth * dt

        self.x += delta_x
        self.y += delta_y
        self.th += delta_th
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, self.th)

        self.broadcaster.sendTransform(
            (1, 1, 0.),
            tf.transformations.quaternion_from_euler(0, 0, 0),
            current_time,
            "odom",
            "map"
        )
        self.broadcaster.sendTransform(
            (self.x, self.y, 0.),
            odom_quat,
            current_time,
            "base_link",
            "odom"
        )
        
        self.broadcaster.sendTransform(
            (0.0, 0.0, 0.4),
            tf.transformations.quaternion_from_euler(0, 0, self.th * 50),
            current_time,
            "lidar_link",
            "base_link"
        )
        odom = Odometry()
        odom.header.stamp = current_time
        odom.header.frame_id = "odom"
        odom.pose.pose = Pose(Point(self.x, self.y, 0.), Quaternion(*odom_quat))

        odom.child_frame_id = "base_link"
        odom.twist.twist = Twist(Vector3(self.vx, self.vy, 0), Vector3(0, 0, self.vth))

        self.odom_pub.publish(odom)
        self.last_time = current_time

    def update(self, msg):
        self.cmd = msg

    def run(self):
        while not rospy.is_shutdown():
            self.new_pos = self.cmd

            self.pub1.publish(self.new_pos)
            self.publica_odom()
            self.rate.sleep()




if __name__ == '__main__':
    try:
        mestre = controle_mestre()
        mestre.run()
    except rospy.ROSInterruptException:
        pass

