Trabalho realizada para disciplina Robótica Movel do curso de Engenharia de Computação UTFPR-PB.
Foi realizado o controle de dois robôs, o primeiro é o mestre, controle dos movimentos utilizando teleop e o segundo segue 
o mestre.

Pré-requisito:
 - ROS
 - Turtlebot3

Intruções de execução:

1 - Clonar este projeto no diretório em que está o turtlebot3

    git clone https://gitlab.com/gvaladao95/Siga_o_Mestre.git

2 - Executar o projeto, iniciar teleop do turtlebot3 e iniciar os nós:

    roslaunch SigaMestre two_tb3.launch
    
    rosrun turtlebot3_teleop turtlebot3_teleop_key

    rosrun SigaMestre Mestre.py

    rosrun SigaMestre seguidor.py
